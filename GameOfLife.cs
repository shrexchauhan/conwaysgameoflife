﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class cell
    {
        
        public int x;
        public int y;
        public bool alive;
        public int count;
        public cell()
        {
            count = 0;
            alive = true;
        }
    }
    public class GameOfLife
    {
        private List<cell> alivecells = new List<cell>();
        private List<cell> deadcells = new List<cell>();
        public GameOfLife()
        {
            alivecells.Clear();
            deadcells.Clear();
        }
        public void init(List<cell> initcell)
        {
            alivecells.Clear();
            alivecells = initcell;
        }
        public void addcell(int x ,int y)
        {
            cell ccc = new cell();
            ccc.x = x; ccc.y = y;
            ccc.alive = true;
            alivecells.Add(ccc);
        }
        private void updatedeadcell(int x ,int y)
        {
            
            foreach (cell c in deadcells)
            {
                if (c.x == x && c.y == y)
                {
                    c.count++;
                    return;
                }
            }
            cell cc = new cell();
            cc.count = 1;
            cc.x = x;
            cc.y = y;
            deadcells.Add(cc);
        }
        public void simulateOneStep()
        {
            int tcount = 0;
            foreach (cell c in alivecells)
            {
                
                tcount = 0;
                foreach (cell ic in alivecells)
                {
                    if ((c.x - 1 == ic.x) && (c.y == ic.y))
                        tcount++;
                    if ((c.x + 1 == ic.x) && (c.y == ic.y))
                        tcount++;
                    if ((c.x  == ic.x) && (c.y -1== ic.y))
                        tcount++;
                    if ((c.x  == ic.x) && (c.y+1 == ic.y))
                        tcount++;
                    if ((c.x - 1 == ic.x) && (c.y-1 == ic.y))
                        tcount++;
                    if ((c.x - 1 == ic.x) && (c.y+1 == ic.y))
                        tcount++;
                    if ((c.x + 1 == ic.x) && (c.y+1 == ic.y))
                        tcount++;
                    if ((c.x + 1 == ic.x) && (c.y -1== ic.y))
                        tcount++;

                }
                if (tcount < 2)
                    c.alive = false;
                if (tcount > 3)
                    c.alive = false;

                updatedeadcell(c.x-1, c.y);
                updatedeadcell(c.x+1, c.y);
                updatedeadcell(c.x, c.y-1);
                updatedeadcell(c.x, c.y+1);
                updatedeadcell(c.x-1, c.y-1);
                updatedeadcell(c.x-1, c.y+1);
                updatedeadcell(c.x+1, c.y-1);
                updatedeadcell(c.x+1, c.y+1);

            }
            foreach (cell dc in deadcells)
            {
                if (dc.count == 3)
                {
                    cell nc = new cell();
                    nc.x = dc.x;
                    nc.y = dc.y;
                    nc.alive = true;
                    alivecells.Add(nc);
                }
            }
            cell[] arr= alivecells.ToArray();
            int ii = 0;
            for (ii = 0; ii < arr.Length; ii++)
            {
                if (arr[ii].alive == false)
                {
                    alivecells.Remove(arr[ii]);
                }
            }
            deadcells.Clear();
        }
        public List<cell> getAliveCells()
        {
            return alivecells;
        }
    


    }
}
