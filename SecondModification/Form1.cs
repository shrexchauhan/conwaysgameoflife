﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameOfLife
{
    public partial class Form1 : Form
    {
        GameOfLife g = new GameOfLife();
        List<cell> cells = new List<cell>();
         Bitmap BackBuffer;
         Graphics formGraphics;
    System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
    System.Drawing.Pen p;

    System.Drawing.Graphics gr;
        
        public Form1()
        {
            InitializeComponent();
            p = new Pen(myBrush);
            this.DoubleBuffered = true;
            gr = this.CreateGraphics();
            BackBuffer = new Bitmap(this.Width, this.Height);
            formGraphics = Graphics.FromImage(BackBuffer);
        }

        private void Form1_Load(object sender, EventArgs e)
        {



            /*g.addcell(20, 21);
                       g.addcell(21, 21);
                       g.addcell(22, 21);
                       g.addcell(21, 20);
                       g.addcell(22, 20);
                       g.addcell(23, 20);
              
               g.addcell(2, 1);
                      g.addcell(3, 2);
                       g.addcell(3, 1);
                       g.addcell(3, 2);
                      g.addcell(3, 3);
              
              g.addcell(22, 11);
                       g.addcell(23, 11);
                       g.addcell(23, 12);
                       g.addcell(22, 12);
         
                       g.addcell(42, 11);
                       g.addcell(43, 11);
                       g.addcell(41, 12);
                       g.addcell(44, 12);
                       g.addcell(42, 13);
                       g.addcell(43, 13);
              
             g.addcell(10, 11);
                       g.addcell(11, 11);
                       g.addcell(12, 11);


            
           g.addcell(22, 1);
                       g.addcell(23, 2);
                       g.addcell(21, 3);
                       g.addcell(22, 3);
                       g.addcell(23, 3);
                    
           */

            g.addcell(2, 1);
                g.addcell(3, 2);
                g.addcell(1, 3);
                g.addcell(2, 3);
                g.addcell(3, 3);


        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            g.simulateOneStep();
            cells = g.getAliveCells();
            int wid = this.Size.Width;
            int hei = this.Size.Height;
           
            
            formGraphics.Clear(Color.White);
           // int col = 0;
            //int row = 0;
            for (int i = 0; i < wid; i += 10)
            {
                formGraphics.DrawLine(p, new Point(i, 0), new Point(i, hei));
            }
                //  col++;
                for (int j = 0; j < hei; j += 10)
                {
                    //   row++;

                    formGraphics.DrawLine(p, new Point(0, j), new Point(wid, j));
                }
            
            
           
            foreach (cell ccc in cells)
            {
                if (ccc.alive)
                {
                    formGraphics.FillRectangle(myBrush, new Rectangle(ccc.x * 10, ccc.y * 10, 10, 10));
                }
            }
            gr.DrawImageUnscaled(BackBuffer,0,0);
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            BackBuffer = new Bitmap(this.Width, this.Height);
            formGraphics = Graphics.FromImage(BackBuffer);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (timer1.Interval > 35)
            {
                timer1.Interval = (int)((float)timer1.Interval *0.75f);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (timer1.Interval < 1000)
            {
                timer1.Interval = (int)((float)timer1.Interval * 1.25f);
            }
        }
    }
}
