﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameOfLife
{
    public partial class Form1 : Form
    {
        GameOfLife g = new GameOfLife();
        List<cell> cells = new List<cell>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
/*
            g.addcell(10, 11);
            g.addcell(11, 11);
            g.addcell(12, 11);

            g.addcell(20, 21);
            g.addcell(21, 21);
            g.addcell(22, 21);
            g.addcell(21, 20);
            g.addcell(22, 20);
            g.addcell(23, 20);


            g.addcell(32, 11);
            g.addcell(33, 11);
            g.addcell(33, 12);
            g.addcell(32, 12);
         
            g.addcell(42, 11);
            g.addcell(43, 11);
            g.addcell(41, 12);
            g.addcell(44, 12);
            g.addcell(42, 13);
            g.addcell(43, 13);
            
*/
            
            g.addcell(2, 1);
            g.addcell(3, 2);
            g.addcell(3, 1);
            g.addcell(3, 2);
            g.addcell(3, 3);
            g.simulateOneStep(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            g.simulateOneStep();
            cells = g.getAliveCells();
            int wid = this.Size.Width;
            int hei = this.Size.Height;
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            System.Drawing.Pen p = new Pen(myBrush);
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            formGraphics.Clear(Color.White);
            int col = 0;
            int row = 0;
            for (int i = 0; i < wid; i += 10)
            {
                //  col++;
                for (int j = 0; j < hei; j += 10)
                {
                    //   row++;
                    foreach (cell cc in cells)
                    {
                        if ((cc.x == row) && (cc.y == col))
                        {
                            //formGraphics.FillRectangle(myBrush, new Rectangle(i, j, 10, 10));
                        }
                    }
                    //
                    formGraphics.DrawRectangle(p, new Rectangle(i, j, 10, 10));
                }
            }

            for (int i = 0; i < wid; i += 10)
            {
                col++;
                for (int j = 0; j < hei; j += 10)
                {
                    row++;
                    foreach (cell cc in cells)
                    {
                        if ((cc.x == row) && (cc.y == col))
                        {
                            formGraphics.FillRectangle(myBrush, new Rectangle(i, j, 10, 10));
                        }
                    }
                    //
                    //formGraphics.DrawRectangle(p, new Rectangle(i, j, 10, 10));
                }
            }
            foreach (cell ccc in cells)
            {
                if (ccc.alive)
                {
                    formGraphics.FillRectangle(myBrush, new Rectangle(ccc.x * 10, ccc.y * 10, 10, 10));
                }
            }
            myBrush.Dispose();
            formGraphics.Dispose();
        }
    }
}
